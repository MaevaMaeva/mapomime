# Exercice Cinema Laravel

## Installation

1. ```composer install```

2. créer le fichier .env à partir du fichier .env.example

à modifier : APP_NAME, DB_DATABASE, DB_USERNAME, DB_PASSWORD

3. ```php artisan key:generate```

(Cela devrait remplir le APP_Key du fichier .env)

4. créer la base de données utf8mb4_unicode_ci dans phpmyadmin

5. Faire les migrations et seedings

6. ```php artisan serve```

#### Migrations et seedings

1. ```php artisan migrate:reset```
(si pas la première fois)
2. ```php artisan migrate```
3. ```php artisan db:seed```

#### Informations

[lien vers asana](https://app.asana.com/0/1199908595615629/board)
