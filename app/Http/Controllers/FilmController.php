<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Film;

class FilmController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    function afficheLaListe(){
        return view('liste', ['liste' => Film::all()]);
 
    }
}
