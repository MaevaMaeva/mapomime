<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Models\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CreateUserController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function loginForm()
    {
        return view('connexion');
    }

    public function login()
    {
        $identifiant = request()->validate([
            'nom_de_compte' => ['required', 'min:8', 'max:16'],
            'email' => ['required', 'email'],
            'password' => ['required', 'min:8', 'max:16',],
        ]);

        $resultat = auth()->attempt([
            'nom_de_compte' => request('nom_de_compte'),
            'email'=> request('email'),
            'password'=> request('password'),       
        ]);
        //$errors = has($resultat['email']);
         if ($resultat)
         {
            return redirect()->route('home')->with('info', 'Vous êtes maintenant connecté.');
         }
         else{
            // if($errors){
            //     dd($resultat);
            // }
            //return redirect()->back('login')->withErrors($validator->errors());
            return redirect()->route('login')->with('info', 'Votre connexion a échoué, veuillez réessayer en vérifiant vos identifiants.');
         }
        // Message d'error si pas bon
        // Si ok, on connecte le user et on le redirige
    }

    public function createUserForm(){

        if (Auth::check()) {
            return redirect()->route('home')->with('info', 'Votre êtes déjà connecté.');   
        }

        return view('inscription');
    }

    public function createUser(){
        
        request()->validate([
            'nom_de_compte' => ['required', 'min:8', 'max:16'],
            'email' => ['required', 'email'],
            'password' => ['required', 'confirmed', 'min:8', 'max:16',],
            'password_confirmation' => ['required',],
        ]);
        
         $utilisateur = User::create([
            'nom_de_compte' => request('nom_de_compte'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'role' => 'utilisateur'
        ]);

        Auth::login($utilisateur);
        
        return redirect()->route('home')->with('info', 'Votre compte a bien été crée. Vous êtes maintenant connecté.');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('home')->with('info', 'Vous êtes maintenant déconnecté.');
    }

}
