<?php

namespace App\Http\Controllers;

use App\Models\Film;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;

class PanierController extends Controller
{
    function affichePanier(){
      if(array_key_exists('panier',$_COOKIE)){
        $panier=json_decode($_COOKIE["panier"]);
      }
      else{
        $panier=[];
      }
      $mesFilms= Film::whereIn('id', $panier)->get();

            // récupérer les id des films dans la BDD pour les ajouter au id dans le panier
      
        return view('panier',['films'=>$mesFilms]);
    }
    
    function ajoutDansCookie(Request $request){
      if(array_key_exists('panier',$_COOKIE)){
        $panier=json_decode($_COOKIE["panier"]);
      }
      else{
        $panier=[];
      }
      $idFilm= $request->input('id');
      array_push($panier, $idFilm);
      setcookie('panier',json_encode($panier));

      return redirect('/panier');
    }
}

// une fonction qui va chercher les éléments dans les id pour les mettre dans une variable
// puis pousse dans la BDD, les variables qui les remplis dans les colonnes de la table emprunt
// 