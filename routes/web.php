<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Http\Controllers\CreateUserController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\PanierController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ROUTE DE L'ACCUEIL
Route::get('/', function () {
        return view('welcome');
})->name('home');

// ROUTE DU PANIER
Route::get('/panier',[PanierController::class,'affichePanier']);
Route::post('/panier',[PanierController::class,'ajoutDansCookie']);
    
Route::get('/inscription', [CreateUserController::class, 'createUserForm'])->name('inscription');
Route::post('/inscription', [CreateUserController::class, 'createUser']);

Route::get('/logout', [CreateUserController::class, 'logout'])->name('logout');

Route::get('/connexion', [CreateUserController::class, 'loginForm'])->name('login');
Route::post('/connexion', [CreateUserController::class, 'login']);

route::get('/utilisateurs', function () {
    
    $utilisateurs = User::all();

     return view('\utilisateurs', [
        'utilisateurs' => $utilisateurs
                        
    ]);
});
    
/*exemple de page :
Route::get('{page}', function($page){
	return 'Bienvenue sur la page ' . $page . ' !'; 
});*/
Route::get('test', function(){
    
    $response = Http::get('http://www.omdbapi.com/?t=titanic&plot=full&apikey=c09289cb');
    print_r("<p>Titre : ".$response['Title']."</p>
    <p>Résumé : ".$response['Plot']."</p>
    <p>Année : ".$response['Year']."</p>"
    );

});

Route::get('liste', [FilmController::class, 'afficheLaListe']);
