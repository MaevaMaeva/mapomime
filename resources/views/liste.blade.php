<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

        <title>Liste</title>
    


<h1>Liste des films</h1>
<div class="row row-cols-1 row-cols-md-2">
    @foreach ($liste as $film)
        
        <div class="col">
            <div class="card h-100">
                <img src="{{$film->poster}}" class="card-img-top" alt="poster du film {{$film->title}}">
                <div class="card-body">
                    <h3 class="card-title">{{$film->title}}</h3>
                    <p class="card-text">Synopsis : {{$film->plot}}</p>
                    <p class="card-text">Type : {{$film->type}}</p>
                    <p class="card-text">{{$film->director}}</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">{{$film->runtime}}</small>
                </div>
                <div class="card-footer">
                    <small class="text-muted">{{$film->year}}</small>
                </div>
                    {{-- AJOUTER UN FILM AU PANIER --}}
                    <form action="/panier" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{$film->id}}" id="" >
                    <button type="submit" onclick="addFilmPanier()" type="button" class="btn btn-secondary">Ajouter au panier</button>
                    </form>
            </div>
        </div>
        
    @endforeach
    </div>