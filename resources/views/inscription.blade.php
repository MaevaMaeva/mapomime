@extends('layout')

@section('content')
<form action="/inscription" method="post">
 {{ csrf_field() }}
        
        <p>
            <label for="nom_de_compte">Veuillez crée votre nom de compte:</label>
        </p>

            <p>
                <input type="text" name="nom_de_compte" placeholder="crée votre nom de compte" required>
            </p>
            @if($errors->has('nom_de_compte'))
                <p>{{ $errors->first('nom_de_compte') }}</p>
            @endif
    
        <p>
            <label for="email">Adresse e-mail:</label>
        </p>

            <p>
                <input type="text" name="email" placeholder="Email" required>
            </p>
            @if($errors->has('email'))
                <p>{{ $errors->first('email') }}</p>
            @endif
        
        <p>
            <label for="Mot de passe">Création du mot de passe:</label>
        </p>

        <p>
            <input type="password" name="password" placeholder="Mot de passe" required>
        </p>  
        @if($errors->has('password'))
            <p>{{ $errors->first('password') }}</p>
        @endif
    
    <p>
        <label for="Confirmation_password">Confirmation du mot de passe:</label>
    </p>

    <p>
        <input type="password" name="password_confirmation" placeholder="Mot de passe (confirmation)" required>
    </p>
    @if($errors->has('password_confirmation'))
        <p>{{ $errors->first('password_confirmation') }}</p>
    @endif

    <p class="button">
        <button type="submit">Validation d'inscription</button>
    </p>
    
</form>
@endsection
