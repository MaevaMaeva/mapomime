@extends('layout')

@section('content')
<form action="/connexion" method="post">
 {{ csrf_field() }}
        
        <p>
            <label for="nom_de_compte">Renseigner votre nom de compte:</label>
        </p>

            <p>
                <input type="text" name="nom_de_compte" placeholder="Votre nom de compte" required>
            </p>
            @if($errors->has('nom_de_compte'))
                <p>{{ $errors->first('nom_de_compte') }}</p>
            @endif

        <p>
            <label for="email">Adresse e-mail:</label>
        </p>

            <p>
                <input type="text" name="email" placeholder="Email" required>
            </p>
            @if($errors->has('email'))
                <p>{{ $errors->first('email') }}</p>
            @endif
        

        <p>
            <label for="Mot de passe">Veuillez saisir votre mot de passe:</label>
        </p>

        <p>
            <input type="password" name="password" placeholder="Mot de passe" required>
        </p>  
        @if($errors->has('password'))
            <p>{{ $errors->first('password') }}</p>
        @endif

    <p class="button">
        <button type="submit">Se connecter</button>
    </p>
    
</form>
@endsection
