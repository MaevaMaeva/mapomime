<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

    </head>
    <body>

        <div class="messages">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has($msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get($msg) }}</p>
                @endif
            @endforeach
        </div> <!-- end .flash-message -->

            <nav>
                @if (Route::has('login'))
                    @auth
                    <a href="{{ route('home') }}">Accueil</a>
                    <a href="{{ route('home') }}">Liste des films</a>
                    <a href="{{ route('logout') }}">Déconnexion</a>
                    @else
                        <a href="{{ route('login') }}">Se connecter</a>

                        @if (Route::has('inscription'))
                            <a href="{{ route('inscription') }}">S'incrire</a>
                        @endif
                    @endauth
                @endif
            </nav>

        @yield('content')
    </body>
</html>

