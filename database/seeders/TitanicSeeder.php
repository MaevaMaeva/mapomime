<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class TitanicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $response = Http::get('http://www.omdbapi.com/?t=titanic&apikey=c09289cb');
        DB::table('films')->insert([
            'title' => $response['Title'],
            'plot' => $response['Plot'],
            'runtime' => $response['Runtime'],
            'year' => $response['Year'],
            'poster' => $response['Poster'],
            'type' => $response['Type'],
            'director' => $response['Director'],        
            'imdbid' => $response['imdbID']
        ]);
    }
}
