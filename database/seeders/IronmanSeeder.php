<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class IronmanSeeder extends Seeder


{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i < 8; $i++) {
            $pagesIM = Http::get('http://www.omdbapi.com/?s=iron-man&page='.$i.'&apikey=c09289cb&');
            foreach($pagesIM['Search'] as $film){
                $response= Http::get('http://www.omdbapi.com/?i='.$film['imdbID'].'&apikey=c09289cb&');

                // Debug
                $this->command->info("Import du film " . $response['Title']);

                DB::table('films')->insert([
                    'title' => $response['Title'],
                    'plot' => $response['Plot'],
                    'runtime' => $response['Runtime'],
                    'year' => $response['Year'],
                    'poster' => $response['Poster'],
                    'type' => $response['Type'],
                    'director' => $response['Director'],        
                    'imdbid' => $response['imdbID']
                ]);
            }
        }
        
        
        
    }
}
